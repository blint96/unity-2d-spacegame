﻿using UnityEngine;
using System.Collections;

public class PlanetRotate : MonoBehaviour {
    public GameObject planet;
    public Vector3 rotate_vec;
    public float transparency = 1.0f;
    public float rotateSpeed = 0.5f;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        planet.transform.Rotate(rotate_vec * Time.deltaTime * rotateSpeed);
    }
}
