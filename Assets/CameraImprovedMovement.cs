﻿using UnityEngine;
using System.Collections;

public class CameraImprovedMovement : MonoBehaviour {
    public Camera cam;
    public float smooth;

    private float x = 0;
    private float y = 0;

	void Start () {
	    
	}
	
	void Update () {
        // Arrow movement
        PerformKeyboardMovement();
	}

    void PerformKeyboardMovement()
    {
        if (Input.GetButton("A"))
        {
            if(x > 0)
            {
                x -= 0.2f;
                cam.transform.position = new Vector3(x, cam.transform.position.y, cam.transform.position.z);
            }
        }

        if (Input.GetButton("D"))
        {
            x += 0.2f;
            cam.transform.position = new Vector3(x, cam.transform.position.y, cam.transform.position.z);
        }

        if (Input.GetButton("W"))
        {
            y += 0.2f;
            cam.transform.position = new Vector3(cam.transform.position.x, y, cam.transform.position.z);
        }

        if (Input.GetButton("S"))
        {
            if(y > 0)
            {
                y -= 0.2f;
                cam.transform.position = new Vector3(cam.transform.position.x, y, cam.transform.position.z);
            }
        }
    }
}
