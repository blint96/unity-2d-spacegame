﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

    public Camera cam;
    public float smooth = 1.5f;
    private Vector3 newPos;
    private Vector3 velocity = Vector3.zero;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 mousePos = Input.mousePosition;

        // If LPM clicked then move cam
        if (Input.GetMouseButton(0))
        {
            velocity.z = 0; // to stop weird objects magnification

            // Move axis Y
            float move_y = 0f;
            if(Input.GetAxis("Mouse Y") > 0)
            {
                move_y = mousePos.y;
            }
            if(Input.GetAxis("Mouse Y") < 0)
            {
                move_y = -mousePos.y;
            }

            // Move axis X
            if(Input.GetAxis("Mouse X") > 0)
            {
                cam.transform.position = Vector3.SmoothDamp(cam.transform.position, new Vector3(mousePos.x, move_y), ref velocity, 10f);
            }
            else if(Input.GetAxis("Mouse X") < 0)
            {
                cam.transform.position = Vector3.SmoothDamp(cam.transform.position,  new Vector3(-mousePos.x, move_y), ref velocity, 10f);
            }
        } 
        else
        {
            velocity = Vector3.zero;
        }
	}
}
